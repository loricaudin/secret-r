#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fonctions.h"

/** la fonction ajouter client permet d'ajouter un client a notre fichier annuaire et il se pisitione a la fin du fichier;
 * les parametre d'entr�e sont: nom_annuaire,nom_p,prenom_p,code_postal_p,ville_p,telephone_p,mel_p,preofession_p.
 * le parametre de sortie est: le fichier annuaire.txt
 * return:la fonction retourne le fichier resultat_ajouter.txt
 */
void ajouter_client(const char *nom_annuaire, const char* nom_p,const char* prenom_p,const char* code_postal_p,const char* ville_p,
const char* telephone_p,const char* mel_p,const char* profession_p)
{
     FILE* fichier_annuaire = fopen(nom_annuaire,"r");
     FILE* resultat_ajouter = fopen("resultat_ajouter.txt","w");
     int i;
     char caractere = fgetc(fichier_annuaire);
     while (!feof(fichier_annuaire))
    {
        fputc(caractere,resultat_ajouter);
        caractere = fgetc(fichier_annuaire);
    }
     for(i=0;nom_p[i]!='\0';i++)
     {
        fputc(nom_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;prenom_p[i]!='\0';i++)
     {
        fputc(prenom_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;code_postal_p[i]!='\0';i++)
     {
        fputc(code_postal_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;ville_p[i]!='\0';i++)
     {
        fputc(ville_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;telephone_p[i]!='\0';i++)
     {
        fputc(telephone_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;mel_p[i]!='\0';i++)
     {
        fputc(mel_p[i],resultat_ajouter);

     }
     fputc(',',resultat_ajouter);
     for(i=0;profession_p[i]!='\0';i++)
     {
        fputc(profession_p[i],resultat_ajouter);

     }
     fputc('\n',resultat_ajouter);

     fclose(resultat_ajouter);
     printf("le client a bien ete ajoute a l'anuaire, les modifications sont visibles dans le fichier resultat_ajouter.txt\n\n");

}

/** Cette fonction permet de'afficher l'annuaire.
 * parametre entre:nom_annuaire.
 * parametre sortie:
 * \return
 */

void afficher_annuaire_clients(const char *nom_annuaire)
{
    FILE* fichier_annuaire = fopen(nom_annuaire, "r");
    int y_annuaire = 500;
    char ligne[y_annuaire];

    printf("Voici le contenu de l'annuaire :\n\n");
    printf("Nom,Prenom,Code-Postal,Ville,Numero de telephone,Mail,Profession\n\n");
    fgets(ligne, y_annuaire, fichier_annuaire);
    while (!feof(fichier_annuaire))
    {
        printf("%s\n", ligne);
        fgets(ligne, y_annuaire, fichier_annuaire);
    }
    fclose(fichier_annuaire);
}
/** Suprimer client permet de suprrimer les infomations d'un client deja enregistr� dans l'annuaire.
 * parametre entr�es:nom_annuaire,mel_p
 * parametre sortie:resultat_suprimer.txt
 * \return
 */
void supprimer_client(const char *nom_annuaire, const char* mel_p)
{
    int i = 0;
    int j, k;
    int n = 1;
    char caractere;
    int est_trouve = 0;
    int len_mel_p = 0;
    int n_virgules = 0;
    FILE* fichier_annuaire = fopen(nom_annuaire, "r");
    int x_annuaire;
    int y_annuaire = 500;
    int cpt;

    for(x_annuaire = 1; !(feof(fichier_annuaire)); caractere = fgetc(fichier_annuaire))
    {
        if(caractere == '\n')
        {
            x_annuaire = x_annuaire + 1;
        }
    }

    fclose(fichier_annuaire);
    fichier_annuaire = fopen(nom_annuaire, "r");

    char **annuaire = (char **)malloc(sizeof(char *) * x_annuaire);
    if(annuaire == NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt=0; cpt < x_annuaire; cpt++)
    {
        annuaire[cpt] = (char *)malloc(sizeof(char) * y_annuaire);
        if(annuaire[cpt] == NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    for(len_mel_p = 0; mel_p[len_mel_p] != '\0'; len_mel_p++);
    while (!feof(fichier_annuaire) && !est_trouve)
    {
        caractere = fgetc(fichier_annuaire);
        if(caractere == ',')
        {
            n_virgules = n_virgules + 1;
        }
        if(caractere == mel_p[0] && n_virgules == 5)
        {
            i = 1;
            do
            {
                est_trouve = 1;
                caractere = fgetc(fichier_annuaire);
                if(caractere != mel_p[i])
                {
                    est_trouve = 0;
                }
                i = i + 1;
            }while(i < len_mel_p && est_trouve);

        }
        else if(caractere == '\n')
        {
            n = n + 1;
            n_virgules = 0;
        }
    }
    fclose(fichier_annuaire);
    if(!est_trouve)
    {
        printf("Erreur : L'adresse est introuvable dans votre annuaire.\n\n");
    }
    else
    {
        fichier_annuaire = fopen(nom_annuaire, "r");
        i = 1;
        k = 0;

        while (!feof(fichier_annuaire))
        {
            if(i != n)
            {
                fgets(annuaire[i-k], y_annuaire, fichier_annuaire);
            }
            else
            {
                fgets(annuaire[i-k], y_annuaire, fichier_annuaire);
                k = 1;
            }
            i = i + 1;

        }
        fclose(fichier_annuaire);

        fichier_annuaire = fopen("resultat_supprimer.txt", "w");
        j = 1;
        while(j < i - 2)
        {
            fputs(annuaire[j], fichier_annuaire);
            j = j + 1;
        }
        fclose(fichier_annuaire);
        printf("Le client avec l'adresse %s a bien ete supprime. Les modifications sont visibles dans le fichier resultat_supprimer.txt\n\n",mel_p);

    }
    for(cpt = 0; cpt < x_annuaire; cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt] = NULL;
    }
    free(annuaire);
}
/** La fonction permet de filtrer un champs dans l'annuaire.
 * parametre entées:nom_champ,val_chaine
 * parametre sorties:
 * \return
 */
void filtrer_un_champ(const char *nom_annuaire, const char* nom_champ, const char* val_chaine)
{
    int num_virgule;
    int n_virgules = 0;
    int len_chaine;
    int n = 1;
    int i, j, k;
    char caractere;

    FILE* fichier_annuaire = fopen(nom_annuaire,"r");
    FILE* nouv_fichier_annuaire;
    int x_annuaire;
    int y_annuaire = 500;

    for(x_annuaire = 1; !(feof(fichier_annuaire)); caractere = fgetc(fichier_annuaire))
    {
        if(caractere == '\n')
        {
            x_annuaire = x_annuaire + 1;
        }
    }

    fclose(fichier_annuaire);
    fichier_annuaire = fopen(nom_annuaire, "r");

    int cpt;
    char *ligne = (char *)malloc(y_annuaire * sizeof(char));
    for(len_chaine=0; val_chaine[len_chaine] != '\0'; len_chaine++);

    int *lignes_filtrees = (int *)calloc(x_annuaire, sizeof(int));
    char **annuaire = (char **)malloc(sizeof(char *) * x_annuaire);
    if(annuaire == NULL || lignes_filtrees == NULL || ligne == NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt = 0; cpt < x_annuaire; cpt++)
    {
        annuaire[cpt] = (char *)malloc(sizeof(char) * y_annuaire);
        if(annuaire[cpt] == NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    if(strcmp(nom_champ, "nom") == 0)
    {
        num_virgule = 0;
    }
    else if(strcmp(nom_champ, "prenom") == 0)
    {
        num_virgule = 1;
    }
    else if(strcmp(nom_champ, "code_postal") == 0)
    {
        num_virgule = 2;
    }
    else if(strcmp(nom_champ,"ville") == 0)
    {
        num_virgule = 3;
    }
    else if(strcmp(nom_champ,"telephone") == 0)
    {
        num_virgule = 4;
    }
    else if(strcmp(nom_champ,"mail") == 0)
    {
        num_virgule = 5;
    }
    else if(strcmp(nom_champ,"profession") == 0)
    {
        num_virgule = 6;
    }
    else
    {
        printf("Erreur : Le champ n'existe pas.\n");
        goto fin_filtrer_un_champ;
    }

    while(!feof(fichier_annuaire))
    {
        caractere = fgetc(fichier_annuaire);
        if(caractere == ',')
        {
            n_virgules = n_virgules + 1;
        }
        if(caractere == val_chaine[0] && n_virgules == num_virgule)
        {
            i = 1;
            if(len_chaine == 1)
            {
                lignes_filtrees[n] = 1;
            }
            else
            {
                do
                {
                    lignes_filtrees[n] = 1;
                    caractere=fgetc(fichier_annuaire);
                    if(caractere != val_chaine[i])
                    {
                        lignes_filtrees[n] = 0;
                    }
                    i = i + 1;
                }while(i < len_chaine && lignes_filtrees[n]);
            }

        }
        else if(caractere == '\n')
        {
            n = n + 1;
            n_virgules = 0;
        }
    }
    fclose(fichier_annuaire);

    fichier_annuaire = fopen(nom_annuaire, "r");
    i = 1;
    fgets(ligne, y_annuaire, fichier_annuaire);
    j = 1;
    while(!feof(fichier_annuaire))
    {
        if(lignes_filtrees[j] == 1)
        {
            strcpy(annuaire[i], ligne);
            i = i + 1;
        }
        j = j + 1;
        fgets(ligne, y_annuaire, fichier_annuaire);
    }
    fclose(fichier_annuaire);
    nouv_fichier_annuaire = fopen("resultat_filtrer_un_champ.txt", "w");
    j = 1;
    while(j < i)
    {
        for(k=0; annuaire[j][k] != '\n'; k++)
        {
            fputc(annuaire[j][k], nouv_fichier_annuaire);
        }
        fputc('\n', nouv_fichier_annuaire);
        j = j + 1;
    }

    fclose(nouv_fichier_annuaire);

    fin_filtrer_un_champ:

    for(cpt = 0; cpt < x_annuaire; cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt] = NULL;
    }
    free(annuaire);
    free(lignes_filtrees);
    free(ligne);

    printf("Tous les clients qui contiennent %s sont sauvegardes dans resultat_filtrer_un_champ.txt\n",val_chaine);

}
/** Cette fonctions permet de de filter deux champs.
 * parametre entrées: nom_champ1,val_chaine,nom_champ2,val_chaine2
 * parametr sorties:
 * \return
 */
void filtrer_combiner_deux_champs(const char *nom_annuaire, const char* nom_champ1,const char* val_chaine1,const char* nom_champ2,const char* val_chaine2)
{
    int num_virgule1, num_virgule2;
    int n_virgules = 0;
    int len_chaine1, len_chaine2;
    int n = 1;
    int i, j, k;
    char caractere;

    FILE* fichier_annuaire = fopen(nom_annuaire,"r");
    FILE* nouv_fichier_annuaire;

    int x_annuaire;
    int y_annuaire=500;

    for(x_annuaire = 1; !(feof(fichier_annuaire)); caractere = fgetc(fichier_annuaire))
    {
        if(caractere == '\n')
        {
            x_annuaire = x_annuaire + 1;
        }
    }

    fclose(fichier_annuaire);
    fichier_annuaire = fopen(nom_annuaire, "r");

    int cpt;
    char *ligne=(char *)malloc(y_annuaire*sizeof(char));

    int *lignes_filtrees=(int *)calloc(x_annuaire,sizeof(int));
    char **annuaire=(char **)malloc(sizeof(char *)*x_annuaire);
    if(annuaire==NULL||lignes_filtrees==NULL||ligne==NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        annuaire[cpt]=(char *)malloc(sizeof(char)*y_annuaire);
        if(annuaire[cpt]==NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    for(len_chaine1=0;val_chaine1[len_chaine1]!='\0';len_chaine1++);
    for(len_chaine2=0;val_chaine2[len_chaine2]!='\0';len_chaine2++);

    if(strcmp(nom_champ1,"nom")==0)
    {
        num_virgule1=0;
    }
    else if(strcmp(nom_champ1,"prenom")==0)
    {
        num_virgule1=1;
    }
    else if(strcmp(nom_champ1,"code_postal")==0)
    {
        num_virgule1=2;
    }
    else if(strcmp(nom_champ1,"ville")==0)
    {
        num_virgule1=3;
    }
    else if(strcmp(nom_champ1,"telephone")==0)
    {
        num_virgule1=4;
    }
    else if(strcmp(nom_champ1,"mail")==0)
    {
        num_virgule1=5;
    }
    else if(strcmp(nom_champ1,"profession")==0)
    {
        num_virgule1=6;
    }
    else
    {
        printf("Erreur : Le champ 1 n'existe pas.\n");
        goto fin_filtrer_combiner_deux_champs;
    }
    if(strcmp(nom_champ2,"nom")==0)
    {
        num_virgule2=0;
    }
    else if(strcmp(nom_champ2,"prenom")==0)
    {
        num_virgule2=1;
    }
    else if(strcmp(nom_champ2,"code_postal")==0)
    {
        num_virgule2=2;
    }
    else if(strcmp(nom_champ2,"ville")==0)
    {
        num_virgule2=3;
    }
    else if(strcmp(nom_champ2,"telephone")==0)
    {
        num_virgule2=4;
    }
    else if(strcmp(nom_champ2,"mail")==0)
    {
        num_virgule2=5;
    }
    else if(strcmp(nom_champ2,"profession")==0)
    {
        num_virgule2=6;
    }
    else
    {
        printf("Erreur : Le champ 2 n'existe pas.\n");
        goto fin_filtrer_combiner_deux_champs;
    }

    while(!feof(fichier_annuaire))
    {
        caractere=fgetc(fichier_annuaire);
        if(caractere==',')
        {
            n_virgules=n_virgules+1;
        }
        if(caractere==val_chaine1[0] && n_virgules==num_virgule1)
        {
            i=1;
            if(len_chaine1==1)
            {
                lignes_filtrees[n]=1;
            }
            else
            {
                do
                {
                    lignes_filtrees[n]=1;
                    caractere=fgetc(fichier_annuaire);
                    if(caractere!=val_chaine1[i])
                    {
                        lignes_filtrees[n]=0;
                    }
                    i=i+1;
                }while(i<len_chaine1 && lignes_filtrees[n]);
            }

        }
        else if(caractere==val_chaine2[0] && n_virgules==num_virgule2)
        {
            i=1;
            if(len_chaine2==1)
            {
                lignes_filtrees[n]=1;
            }
            else
            {
                do
                {
                    lignes_filtrees[n]=1;
                    caractere=fgetc(fichier_annuaire);
                    if(caractere!=val_chaine2[i])
                    {
                        lignes_filtrees[n]=0;
                    }
                    i=i+1;
                }while(i<len_chaine2 && lignes_filtrees[n]);
            }

        }
        else if(caractere=='\n')
        {
            n=n+1;
            n_virgules=0;
        }
    }
    fclose(fichier_annuaire);

    fichier_annuaire = fopen(nom_annuaire,"r");
    i=1;
    fgets(ligne,500,fichier_annuaire);
    j=1;
    while(!feof(fichier_annuaire))
    {
        if(lignes_filtrees[j]==1)
        {
            strcpy(annuaire[i],ligne);
            i=i+1;
        }
        j=j+1;
        fgets(ligne,500,fichier_annuaire);
    }
    fclose(fichier_annuaire);
    nouv_fichier_annuaire = fopen("resultat_filtrer_combiner_deux_champs.txt","w");
    j=1;
    while(j<i)
    {
        for(k=0;annuaire[j][k]!='\n';k++)
        {
            fputc(annuaire[j][k],nouv_fichier_annuaire);
        }
        fputc('\n',nouv_fichier_annuaire);
        j=j+1;
    }

    fclose(nouv_fichier_annuaire);

    fin_filtrer_combiner_deux_champs:

    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt]=NULL;
    }
    free(annuaire);
    free(lignes_filtrees);
    free(ligne);

    printf("Tous les clients qui contiennent %s et %s sont sauvegardes dans resultat_filtrer_combiner_deux_champs.txt\n",val_chaine1,val_chaine2);

}
/** La fonction midifier autre que mel client permet de modifier un autre champ que le mel d'un client deja enregistr� dans l'annuaire.
 * parametre entr�es:nom_annuaire,mel_p,bom_champ,nv_valeur.
 * parametre sortie:resultat_midifier_autre_que_mel.txt
 * \return
 */

void modifier_autre_que_mel_client(const char *nom_annuaire, const char* mel_p,const char* nom_champ,const char* nv_valeur)
{
    int i;
    int j, k;
    int n=1;
    char caractere;
    int est_trouve=0;
    int len_mel_p=0;
    int n_virgules=0;
    int num_virgule;
    FILE* fichier_annuaire = fopen(nom_annuaire,"r");

    int x_annuaire;
    int y_annuaire=500;

    for(x_annuaire = 1; !(feof(fichier_annuaire)); caractere = fgetc(fichier_annuaire))
    {
        if(caractere == '\n')
        {
            x_annuaire = x_annuaire + 1;
        }
    }

    fclose(fichier_annuaire);
    fichier_annuaire = fopen(nom_annuaire, "r");

    int cpt;
    char **annuaire=(char **)malloc(sizeof(char *)*x_annuaire);
    if(annuaire==NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        annuaire[cpt]=(char *)malloc(sizeof(char)*y_annuaire);
        if(annuaire[cpt]==NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    for(len_mel_p=0;mel_p[len_mel_p]!='\0';len_mel_p++);

    if(strcmp(nom_champ,"nom")==0)
    {
        num_virgule=0;
    }
    else if(strcmp(nom_champ,"prenom")==0)
    {
        num_virgule=1;
    }
    else if(strcmp(nom_champ,"code_postal")==0)
    {
        num_virgule=2;
    }
    else if(strcmp(nom_champ,"ville")==0)
    {
        num_virgule=3;
    }
    else if(strcmp(nom_champ,"telephone")==0)
    {
        num_virgule=4;
    }
    else if(strcmp(nom_champ,"mail")==0)
    {
        printf("Erreur : Vous devez utiliser la fonctionnalite : Modifier un mail pour modifier le champ mail.\n\n");
        goto fin_modifier_autre_que_mel_client;
    }
    else if(strcmp(nom_champ,"profession")==0)
    {
        num_virgule=6;
    }
    else
    {
        printf("Erreur : Le champ n'existe pas.\n\n");
        goto fin_modifier_autre_que_mel_client;
    }

    while (!feof(fichier_annuaire) && !est_trouve)
    {
        caractere=fgetc(fichier_annuaire);
        if(caractere==',')
        {
            n_virgules=n_virgules+1;
        }

        if(caractere==mel_p[0] && n_virgules==5)
        {
            i=1;
            do
            {
                est_trouve=1;
                caractere=fgetc(fichier_annuaire);
                if(caractere!=mel_p[i])
                {
                    est_trouve=0;
                }
                i=i+1;
            }while(i<len_mel_p && est_trouve);

        }
        else if(caractere=='\n')
        {
            n=n+1;
            n_virgules=0;
        }
    }
    fclose(fichier_annuaire);
    if(!est_trouve)
    {
        printf("Erreur : L'adresse est introuvable dans votre annuaire.\n\n");
    }
    else
    {
        fichier_annuaire = fopen(nom_annuaire,"r");
        i=1;
        while (!feof(fichier_annuaire))
        {
            if(i!=n)
            {
                fgets(annuaire[i],500,fichier_annuaire);
            }
            else
            {
                j=0;
                n_virgules=0;
                caractere=fgetc(fichier_annuaire);
                while(!feof(fichier_annuaire) && caractere!='\n')
                {
                    if(caractere==',')
                        n_virgules=n_virgules+1;
                    if(n_virgules==num_virgule)
                    {
                        if(num_virgule!=0)
                        {
                            annuaire[i][j]=caractere;
                            j=j+1;
                        }
                        k=0;
                        while(nv_valeur[k]!='\0')
                        {
                            annuaire[i][j]=nv_valeur[k];
                            k=k+1;
                            j=j+1;

                        }
                        do
                        {
                            caractere=fgetc(fichier_annuaire);

                        }while(caractere!=',' && caractere!='\n');
                        if(caractere=='\n')
                        {
                            break;
                        }
                        annuaire[i][j]=caractere;
                        n_virgules=n_virgules+1;
                    }
                    annuaire[i][j]=caractere;
                    j=j+1;
                    caractere=fgetc(fichier_annuaire);
                }
                annuaire[i][j]='\n';
            }
            i=i+1;
        }
        fclose(fichier_annuaire);
        fichier_annuaire = fopen("resultat_modifier_autre_que_mel.txt","w");
        j=1;
        while(j<i-1)
        {
            for(k=0;annuaire[j][k]!='\n';k++)
            {
                fputc(annuaire[j][k],fichier_annuaire);
            }
            fputc('\n',fichier_annuaire);
            j=j+1;
        }
        fclose(fichier_annuaire);
        printf("Le client a bien ete modifie. Les modifications sont visibles dans le fichier resultat_modifier_autre_que_mel\n\n");
    }

    fin_modifier_autre_que_mel_client:

    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt]=NULL;
    }
    free(annuaire);
}
/** cette fonction permet de midifier le mel d'un client qui est deja enregistr� sur l'annuaire.
 * parametre entr�es:nom_annuaire,mel_p,nv_mel_p.
 * parametre sortie:resultat_modifier_mel.txt
 * \return
 */
void modifier_mel_client(const char *nom_annuaire, const char* mel_p,const char* nv_mel_p)
{
    int i=0;
    int j, k;
    int n=1;
    char caractere;
    int est_trouve=0;
    int len_mel_p=0;
    int n_virgules=0;
    FILE* fichier_annuaire = fopen(nom_annuaire,"r");

    int x_annuaire;
    int y_annuaire=500;

    for(x_annuaire = 1; !(feof(fichier_annuaire)); caractere = fgetc(fichier_annuaire))
    {
        if(caractere == '\n')
        {
            x_annuaire = x_annuaire + 1;
        }
    }

    fclose(fichier_annuaire);
    fichier_annuaire = fopen(nom_annuaire, "r");

    int cpt;
    char **annuaire=(char **)malloc(sizeof(char *)*x_annuaire);
    if(annuaire==NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        annuaire[cpt]=(char *)malloc(sizeof(char)*y_annuaire);
        if(annuaire[cpt]==NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    for(len_mel_p=0;mel_p[len_mel_p]!='\0';len_mel_p++);
    while (!feof(fichier_annuaire) && !est_trouve)
    {
        caractere=fgetc(fichier_annuaire);
        if(caractere==',')
        {
            n_virgules=n_virgules+1;
        }
        if(caractere==mel_p[0] && n_virgules==5)
        {
            i=1;
            do
            {
                est_trouve=1;
                caractere=fgetc(fichier_annuaire);
                if(caractere!=mel_p[i])
                {
                    est_trouve=0;
                }
                i=i+1;
            }while(i<len_mel_p && est_trouve);

        }
        else if(caractere=='\n')
        {
            n=n+1;
            n_virgules=0;
        }
    }
    fclose(fichier_annuaire);
    if(!est_trouve)
    {
        printf("Erreur : L'adresse est introuvable dans votre annuaire.\n\n");
    }
    else
    {
        fichier_annuaire = fopen(nom_annuaire,"r");
        i=1;
        while (!feof(fichier_annuaire))
        {
            if(i!=n)
            {
                fgets(annuaire[i],500,fichier_annuaire);
            }
            else
            {
                j=0;
                n_virgules=0;
                caractere=fgetc(fichier_annuaire);
                while(!feof(fichier_annuaire) && caractere!='\n')
                {
                    if(caractere==',')
                        n_virgules=n_virgules+1;
                    if(n_virgules==5)
                    {
                        annuaire[i][j]=caractere;
                        k=0;
                        j=j+1;
                        while(nv_mel_p[k]!='\0')
                        {
                            annuaire[i][j]=nv_mel_p[k];
                            k=k+1;
                            j=j+1;

                        }
                        do
                        {
                            caractere=fgetc(fichier_annuaire);

                        }while(caractere!=',');
                        annuaire[i][j]=caractere;
                        n_virgules=n_virgules+1;
                    }
                    annuaire[i][j]=caractere;
                    j=j+1;
                    caractere=fgetc(fichier_annuaire);
                }
                annuaire[i][j]='\n';
            }
            i=i+1;
        }
        fclose(fichier_annuaire);
        fichier_annuaire = fopen("resultat_modifier_mel.txt","w");
        j=1;
        while(j<i-1)
        {
            for(k=0;annuaire[j][k]!='\n';k++)
            {
                fputc(annuaire[j][k],fichier_annuaire);
            }
            fputc('\n',fichier_annuaire);
            j=j+1;
        }
        fclose(fichier_annuaire);
        printf("Le client a bien ete modifie. Les modifications sont visibles dans le fichier resultat_modifier_mel.txt\n\n");
    }

    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt]=NULL;
    }
    free(annuaire);

}

/** la fonction ecrire annuaire client permet d'ajouter un nouveau client dans l'annuaire.
 * parametres entr�es:nom_annauires
 * \param
 * \return
 */
void ecriture_annuaire_clients(const char *nom_annuaire)
{
    int i;
    int j;
    int est_finie=0;
    int nb_virgules;

    int x_annuaire=1000;
    int y_annuaire=500;

    int cpt;

    FILE *fichier_annuaire;

    char **annuaire=(char **)malloc(sizeof(char *)*x_annuaire);
    if(annuaire==NULL)
    {
        printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
        exit(EXIT_FAILURE);
    }
    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        annuaire[cpt]=(char *)malloc(sizeof(char)*y_annuaire);
        if(annuaire[cpt]==NULL)
        {
            printf("Erreur critique : Impossible de trouver de l'espace a allouer.");
            exit(EXIT_FAILURE);
        }
    }

    printf("Creation d'un nouvel annuaire :\n");

    fichier_annuaire = fopen("resultat_ecriture.txt","w");
    printf("Entrer l enregistrement a inserer : une fois finie, entrez [exit]\n");
    printf("Entrer vos lignes sous la forme : [nom,prenom,code_postal,ville,telephone,mail,profession] : \n");
    for(i=0; (i<1000 && !est_finie); i++)
    {
        fgets(annuaire[i], 500, stdin); /*Equivalent du scanf, mais avec la prise en compte des espaces*/
        if(i==0)
        {
            i=i+1;
            continue; /*Le premier appel de fgets est ignore car le scanf precedemment appele reprend la valeur entree dans la console*/
        }
        if(annuaire[i][0]!='e'||annuaire[i][1]!='x'||annuaire[i][2]!='i'||annuaire[i][3]!='t')
        {
            j=0;
            nb_virgules=0;
            while(annuaire[i][j]!='\0'&&annuaire[i][j]!='\n')
            {
                if(annuaire[i][j]==',')
                {
                    nb_virgules=nb_virgules+1;
                }
                j=j+1;
            }
            j=0;
            if(nb_virgules==6)
            {
                while(annuaire[i][j]!='\0'&&annuaire[i][j]!='\n')
                {
                    fputc(annuaire[i][j],fichier_annuaire);
                    j=j+1;
                }
                fputc('\n',fichier_annuaire);
            }
            else
            {
                printf("\nErreur : veuillez entrer vos lignes sous la forme : [nom,prenom,code_postal,ville,telephone,mail,profession]\nAstuce : Vous devez avoir saisi 6 virgules\n");
                i=i-1;
            }
            printf("\n");
        }
        else
        {
            est_finie=1;
            printf("\nFin de l'ecriture du fichier. Le fichier a ete ecrit dans resultat_ecriture.txt\n");
        }

        fclose(fichier_annuaire);
    }

    for(cpt=0;cpt<x_annuaire;cpt++)
    {
        free(annuaire[cpt]);
        annuaire[cpt]=NULL;
    }
    free(annuaire);
}

/** Cette fonction sert a saubegarder l'annuaire et l'ecrire dans un fichier_sauvegarde.
 * parametre entrées:nom_annuaire,fichier_modifie.
 * parametre sortie:fichier_sauvegarde.

*/


void sauvegarder_annuaire(const char *nom_annuaire, const char *fichier_modifie)
{
    FILE* fichier_annuaire = fopen(nom_annuaire, "w");
    FILE* fichier_sauvegarde = fopen(fichier_modifie, "r");
    char caractere = fgetc(fichier_sauvegarde);
    while(!feof(fichier_sauvegarde))
    {
        fputc(caractere,fichier_annuaire);
        caractere = fgetc(fichier_sauvegarde);
    }
    fclose(fichier_annuaire);
    fclose(fichier_sauvegarde);
    printf("\nLes modifications ont ete enregistrees.\n");
}
