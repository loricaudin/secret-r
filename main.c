#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"
#include <string.h>

int main(void)
{
    char nom_fichier[20] = "annuaire.csv";
    int choix = -1;
    char cible[100];
    char champ[10];
    char remplacement[100];
    char nom_p[100], prenom_p[100], code_postal_p[10], ville_p[50], telephone_p[15], mel_p[100], profession_p[100];
    char champ1[500], champ2[500], val1[500], val2[500];

    FILE* fichier_annuaire = fopen(nom_fichier, "r"); /*ouverture du fichier*/
    if(!fichier_annuaire)/*si le fichier annuaire.csv n'existe pas, alors on cr�e un nouveau fichier*/
    {
        FILE* fichier = fopen(nom_fichier, "w");
        fclose(fichier);
    }
    fclose(fichier_annuaire);/*fermeture du fichier*/


    printf("#############################################\n");
    printf("#                                           #\n");
    printf("#                  Secret-R                 #\n");
    printf("#                                           #\n");
    printf("#############################################\n");
    printf("\nBienvenue dans Secret-R, votre logiciel de gestion d'annuaire !\n");
    while(choix != 0)
    {
        printf("\nListe des fonctionnalites :\n");
        printf("[1] : Ecriture dans l'annuaire\n");
        printf("[2] : Afficher l'annuaire\n");
        printf("[3] : Ajouter un client\n");
        printf("[4] : Supprimer un client\n");
        printf("[5] : Modifier un mail\n");
        printf("[6] : Modifier une valeur autre que le mail\n");
        printf("[7] : Filtrer un champ\n");
        printf("[8] : Filtrer deux champs\n");
        printf("[0] : Fermer\n");

        printf("\nEntrer le numero de la fonctionnalite de votre choix : ");
        scanf("%d", &choix);
        printf("\nVous avez entre : %d\n\n", choix);
        if(choix == 1)
        {
            ecriture_annuaire_clients(nom_fichier);

            printf("\nVoulez-vous ecraser l ancien fichier annuaire ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d",&choix);
            if(choix==1)
            {
                sauvegarder_annuaire(nom_fichier,"resultat_ecriture.txt");
            }
        }
        else if(choix == 2)
        {
            afficher_annuaire_clients(nom_fichier);
        }
        else if(choix == 3)
        {
            printf("Ajout d'un client :\n");
            printf("Entrer le nom de la personne : ");
            scanf("%s", &nom_p);
            printf("Entrer le prenom de la personne : ");
            scanf("%s", &prenom_p);
            printf("Entrer le code postal de la personne : ");
            scanf("%s", &code_postal_p);
            printf("Entrer la ville de la personne : ");
            scanf("%s", &ville_p);
            printf("Entrer le nunmero de telephone de la personne de preference sous la forme : [xx.xx.xx.xx.xx] : ");
            scanf("%s", &telephone_p);
            printf("Entrer l'adresse mail de la personne : ");
            scanf("%s", &mel_p);
            printf("Entrer la profession de la personne : ");
            scanf("%s", &profession_p);

            ajouter_client(nom_fichier, nom_p, prenom_p, code_postal_p, ville_p, telephone_p, mel_p, profession_p);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix == 1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_ajouter.txt");
            }
        }
        else if(choix == 4)
        {
            printf("Entrer l'adresse du client a supprimer : ");
            scanf("%s", &cible);
            supprimer_client(nom_fichier, cible);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix == 1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_supprimer.txt");
            }
        }
        else if(choix == 5)
        {
            printf("Entrer l'adresse du client a modifier (si vous ne vous souvennez plus de l'adresse, entrez ce que doit contenir l'adresse recherche) : ");
            scanf("%s", &cible);
            printf("Entrer la nouvelle adresse du client : ");
            scanf("%s", &remplacement);
            modifier_mel_client(nom_fichier, cible, remplacement);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix == 1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_modifier_mel.txt");
            }
        }
        else if(choix == 6)
        {
            printf("Entrer l'adresse du client que vous souhaitez modifier (si vous ne vous souvennez plus de l'adresse, entrez ce que doit contenir l'adresse recherche) : ");
            scanf("%s", &cible);
            printf("Quel champ souhaitez-vous modifier ? [nom/prenom/code_postal/ville/telephone/profession]\n");
            scanf("%s", &champ);
            printf("Entrer la nouvelle valeur du champ %s : ", champ);
            scanf("%s", &remplacement);
            modifier_autre_que_mel_client(nom_fichier, cible, champ, remplacement);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix == 1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_modifier_autre_que_mel.txt");
            }
        }
        else if(choix == 7)
        {
            printf("Quel champ souhaitez-vous filtrer ? [nom/prenom/code_postal/ville/telephone/mail/profession]\n");
            scanf("%s", &champ1);
            printf("Entrer la valeur recherchee du champ %s : ",champ1);
            scanf("%s", &val1);
            filtrer_un_champ(nom_fichier, champ1, val1);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix==1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_filtrer_un_champ.txt");
            }
        }
        else if(choix == 8)
        {
            printf("Quel champ souhaitez-vous filtrer ? [nom/prenom/code_postal/ville/telephone/mail/profession]\n");
            scanf("%s", &champ1);
            printf("Entrer la valeur recherchee du champ %s : ",champ2);
            scanf("%s", &val1);
            printf("Quel champ souhaitez-vous egalement filtrer ? [nom/prenom/code_postal/ville/telephone/mail/profession]\n");
            scanf("%s", &champ2);
            printf("Entrer la valeur recherchee du champ %s : ",champ2);
            scanf("%s", &val2);
            filtrer_combiner_deux_champs(nom_fichier,champ1,val1,champ2,val2);

            printf("\nVoulez-vous sauvegarder les modifications ? [1] : Oui; [0] : Quitter; [2] : Non, continuer\n");
            scanf("%d", &choix);
            if(choix==1)
            {
                sauvegarder_annuaire(nom_fichier, "resultat_filtrer_combiner_deux_champs.txt");
            }
        }
        else if(choix == 0)
        {
            printf("Vous quittez ce logiciel.\n");
        }
        else
        {
            printf("Erreur : Vous n'avez pas entre un chiffre entre 0 et 8.\n\n");
        }
    }
    printf("Merci d'avoir utilise ce logiciel.");

    return 0;
}
