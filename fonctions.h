#ifndef FONCTIONS_H
#define FONCTIONS_H

void ajouter_client(const char *nom_annuaire, const char* nom_p, const char* prenom_p, const char* code_postal_p, const char* ville_p,
                    const char* telephone_p, const char* mel_p, const char* profession_p);

void afficher_annuaire_clients(const char *nom_annuaire);

void ecriture_annuaire_clients(const char *nom_annuaire);

void modifier_mel_client(const char *nom_annuaire, const char* mel_p, const char* nv_mel_p);

void modifier_autre_que_mel_client(const char *nom_annuaire, const char* mel_p, const char* nom_champ, const char* nv_valeur);

void filtrer_un_champ(const char *nom_annuaire, const char* nom_champ,const char* val_chaine);

void filtrer_combiner_deux_champs(const char *nom_annuaire, const char* nom_champ1, const char* val_chaine1,
                                  const char* nom_champ2, const char* val_chaine2);

void supprimer_client(const char *nom_annuaire, const char* mel_p);

void sauvegarder_annuaire(const char *nom_annuaire, const char *fichier_modifie);

#endif
